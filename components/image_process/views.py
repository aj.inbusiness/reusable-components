from django.shortcuts import render
from django.views.generic.edit import FormView
from image_process import forms
from django.views import View
from django.urls import reverse_lazy, reverse
from django.shortcuts import render, redirect, loader
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from accounts.models import  User
from .models import UploadImage
from .processors import image_reader
from django.conf import settings
import os

class UploadPhotoView(View):
	form_class = forms.UploadPhotoForm
	template_name = "upload_photo.html"
	success_url = reverse_lazy("image_process:predict_face")
	
	def get(self, request):
		#return HttpResponse(os.path.dirname(os.path.abspath(__file__)))
		form = forms.UploadPhotoForm()
		context = {'form':form}
		return render(request, template_name=self.template_name, context={'form':form})

	def post(self, request):
		form = forms.UploadPhotoForm(request.POST, request.FILES)
		if form.is_valid():
			if request.user.is_authenticated:
				user = User.objects.get(id=request.user.id)
			else:
				user = None
			'''
			form_model = UploadImage(image=request.FILES['image'], user=user)
			form_model.save()
			stored_image = UploadImage.objects.get(id=form_model.id)
			input_image_path = stored_image.image.url
			'''
			app_name = request.resolver_match.app_name
			output_image = image_reader(request.FILES['image'].read(), app_name)
			#os.remove(settings.BASE_DIR+input_image_path)
			#os.remove(settings.BASE_DIR+output_image_path)
			#stored_image.delete()
			return JsonResponse({'image_url':output_image})
		else:
			error_messages = {
					"1":"Please upload valid image", 
					"2":"Image size should be lesser than 1070 X 720"
					}
			return JsonResponse({"error":error_messages})
	'''
	def form_valid(self, form):
		image = self.request.FILES['upload_photo']
		template = loader.get_template("result.html")
		context = self.get_result()
		return HttpResponse(template.render(context, self.request))
	'''
	def get_result(self):
		return {'result': {'gender':'Male', 'age': 25} }

