from django.core.exceptions import ValidationError
import os 
from django.core.files.images import get_image_dimensions
import cv2 as opencv

class FileValidator(object):

	def __init__(self, *args, **kwargs):
		self.allowed_ext = kwargs.pop('allowed_ext', None)
		self.max_width = kwargs.pop('max_width', None)
		self.max_height = kwargs.pop('max_height', None)

	def check_file_extenstion(self, file):
		fileName, fileExt = os.path.splitext(file.name)
		error_message = 'Please upload a valid image.'
		if self.allowed_ext:
			if not fileExt in self.allowed_ext:
				raise ValidationError(error_message)
		else:
			if not fileExt in ['.png', '.jpeg', '.jpg', '.gif']:
				raise ValidationError(error_message)

	def check_width_height(self, file):
		image_width, image_height = get_image_dimensions(file)
		error_message = "Please upload the image lesser than {0} x {1}".format(self.max_width, self.max_height)
		if self.max_width and self.max_height:
			if image_width > self.max_width or image_height > self.max_height:
				raise ValidationError(error_message)
		else:
			if image_width > 1070 or image_height > 530:
				raise ValidationError(error_message)
