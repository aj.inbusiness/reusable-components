import cv2
import numpy as np
import os
from django.conf import settings
import datetime
import io
import base64

def predictAge(face, base_path):
	
	AGE_BUCKETS = ["(0-2)", "(4-6)", "(8-12)", "(15-20)", "(25-32)", "(38-43)", "(48-53)", "(60-100)"]
	# load our serialized age detector model from disk
	prototxtPath = base_path+"/detector/age_detector/age_deploy.prototxt"
	weightsPath = base_path+"	/detector/age_detector/age_net.caffemodel"
	ageNet = cv2.dnn.readNetFromCaffe(prototxtPath, weightsPath)

	# make predictions on the age and find the age bucket with
	# the largest corresponding probability
	faceBlob = cv2.dnn.blobFromImage(face, 1.0, (227, 227), (78.4263377603, 87.7689143744, 114.895847746), swapRB=False)
	ageNet.setInput(faceBlob)
	preds = ageNet.forward()
	i = preds[0].argmax()
	age = AGE_BUCKETS[i]
	ageConfidence = preds[0][i]

	# display the predicted age to our terminal
		
	return (age, ageConfidence * 100)

def predictGender(faceBlob, base_path):
	genderProto = base_path+"/detector/gender_detector/gender_deploy.prototxt"
	genderModel = base_path+"/detector/gender_detector/gender_net.caffemodel"
	
	genderNet = cv2.dnn.readNet(genderModel, genderProto)

	genderList = ['Male', 'Female']
	MODEL_MEAN_VALUES = (78.4263377603, 87.7689143744, 114.895847746)
	blob = cv2.dnn.blobFromImage(faceBlob, 1, (227, 227), MODEL_MEAN_VALUES, swapRB=False)

	genderNet.setInput(blob)
	genderPreds = genderNet.forward()
	gender = genderList[genderPreds[0].argmax()]

	return gender

def predictFace(base_path):
	prototxtPath = base_path+"/detector/face_detector/face_deploy.prototxt"
	weightsPath = base_path+"/detector/face_detector/face_net.caffemodel"
	faceNet = cv2.dnn.readNetFromCaffe(prototxtPath, weightsPath)

	return faceNet

def image_reader(input_image, app_name):
	# define the list of age buckets our age detector will predict
	base_path = os.path.dirname(os.path.abspath(__file__))

	# load the input image and construct an input blob for the image
	image = cv2.imdecode(np.fromstring(input_image, np.uint8), cv2.IMREAD_UNCHANGED)
	#image = cv2.imread(settings.BASE_DIR + input_image, 1)
	if image.shape[1] < 1070 and image.shape[0] < 1070:
		resized_image = image
	else:
		resized_image = cv2.resize(image, (int(image.shape[1]/2), int(image.shape[0]/2)))
	(h, w) = resized_image.shape[:2]

	blob = cv2.dnn.blobFromImage(resized_image, 1.0, (300, 300),(104.0, 177.0, 123.0))
	# pass the blob through the network and obtain the face detections
	#prototxtPath = "image_process/age_detector/age_deploy.prototxt"

	faceNet= predictFace(base_path)
	faceNet.setInput(blob)
	detections = faceNet.forward()

	for i in range(0, detections.shape[2]):
		# extract the confidence (i.e., probability) associated with the
		# prediction
		confidence = detections[0, 0, i, 2]
		default_confidence = 0.5

		# filter out weak detections by ensuring the confidence is
		# greater than the minimum confidence
		if confidence > default_confidence:
			# compute the (x, y)-coordinates of the bounding box for the
			# object
			box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
			(startX, startY, endX, endY) = box.astype("int")

			# extract the ROI of the face and then construct a blob from
			# *only* the face ROI
			face = resized_image[startY:endY, startX:endX]
			age, ageConfidence = predictAge(face, base_path)
			gender = predictGender(face, base_path)
			text = "{1}{0}".format(age, gender)
			# draw the bounding box of the face along with the associated
			# predicted age
			y = startY - 10 if startY - 10 > 10 else startY + 10
			cv2.rectangle(resized_image, (startX, startY), (endX, endY),(0, 255, 255), 2)
			cv2.putText(resized_image, text, (startX, y), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255), 2)
	'''	
	filename = (str(datetime.datetime.now().timestamp()).replace(".","_")) + ".png"
	path = "/static/images/age_gender_detection/output_images/" + filename
	save_to_path = "image_process" + path
	cv2.imwrite(save_to_path, resized_image)
	'''
	encoded_image = base64.b64encode(cv2.imencode('.jpg', resized_image)[1]).decode()
	return encoded_image
