from django import forms
from django.forms import ModelForm
import cv2 as opencv
from .validators import FileValidator
from .models import UploadImage

class UploadPhotoForm(ModelForm):
	#upload_photo = forms.ImageField(label="Upload your photo", error_messages={'invalid_image':'Please upload a valid image'})
	#name = forms.CharField()
	class Meta:
		model = UploadImage
		fields = ('image',)

	def clean_image(self):
		image = self.cleaned_data.get('image')
		#FileValidator(max_width=4000, max_height=4000).check_width_height(image)
		#FileValidator().check_file_extenstion(upload_photo)
		return image
