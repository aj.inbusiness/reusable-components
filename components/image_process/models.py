from django.db import models
import os 
import datetime
from django.conf import settings
from django.core.files.storage import FileSystemStorage

def user_directory_path(instance, filename):
	timestampe = str(datetime.datetime.now().timestamp()).replace(".","_")
	fname = "image_"+ timestampe
	name, ext = os.path.splitext(filename)
	file_path = str(fname) + str(ext)
	return "static/images/age_gender_detection/input_images/{0}".format(file_path)

class UploadImage(models.Model):
	upload_storage = FileSystemStorage(location="image_process", base_url='/image_process')
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
	image = models.ImageField(upload_to=user_directory_path, storage=upload_storage, default=None)
	created_on = models.DateTimeField(auto_now_add=True)