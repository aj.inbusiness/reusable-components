from django.contrib import admin
from django.urls import path, include
from image_process import views
from django.views.generic.base import TemplateView

app_name= 'image_process'

urlpatterns = [
    path('human-face-age-gender/', views.UploadPhotoView.as_view(), name='predict_face'),
]