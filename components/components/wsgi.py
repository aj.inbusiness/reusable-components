"""
WSGI config for components project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os, site, sys

#site.addsitedir('/home/ajith/Documents/reusable-components/venv/lib/python3.6/site-packages')

#sys.path.append('/home/ajith/Documents/reusable-components/components/components')

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'components.settings')

application = get_wsgi_application()
