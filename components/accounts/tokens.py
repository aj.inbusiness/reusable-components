from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from datetime import datetime
from .models import User, UserSecretToken


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
	'''    def _make_hash_value(self, user, timestamp):
	        return (
	            six.text_type(user.pk) + six.text_type(timestamp) +
	            six.text_type(user.profile.email_confirmed)
	        )
	'''
	
	def make_secret_token(self, user):
		timestamp = urlsafe_base64_encode(force_bytes(datetime.now().timestamp()))
		user_token =urlsafe_base64_encode(force_bytes(user.pk))
		secret_token = str(user_token) + str(timestamp)
		uid = user.pk
		if UserSecretToken.objects.filter(user_id=uid).exists():
			secret_token_model = UserSecretToken.objects.get(user_id=uid)
			secret_token_model.secret_token = secret_token
			secret_token_model.is_expired = False
			secret_token_model.resend_count = secret_token_model.resend_count + 1
			secret_token_model.save()
		else:
			secret_token_model = UserSecretToken(secret_token=secret_token, user_id=uid, is_expired=False)
			secret_token_model.save()
		return secret_token

	def check_secret_token(self, uid, secret_token):
		if UserSecretToken.objects.filter(user_id=uid, secret_token=secret_token, is_expired=False).exists():
			return True
		else:
			return False

account_activation_token = AccountActivationTokenGenerator()