from django.utils.translation import gettext as _
from django.urls import reverse
from django.urls import reverse_lazy

class AppMessage:

    def __init__(self):
        pass

    def errorMessages(self, code, *args, **kwargs):
        #username = kwargs.get('username', None)
        resend_link = "<a href='{0}'>Resend Mail Again</a>".format("/accounts/resend/confirm-email/"+kwargs['username']) if 'username' in kwargs else ''

        errors = {
            "first_name_min_length": _("First name should be at least 3 characters long"),
            "first_name_max_length": _("First name should be less than 50 characters"),
            "username_min_length": _("Username should be at least 5 characters long"),
            "username_max_length": _("Username should be less than 50 characters"),
            "last_name_max_length": _("Last name should be less than 30 characters"),
            "username_exists": _("This username isn't available. Please try another"),
            "invalid_username": _("Invlaid username"),
            "email_exists": _("This email address isn't available. Please try another"),
            "password_mismatch": _("Password and Confirm Password does't match."),
            "only_letters": _("Only lettes are allowed"),
            "first_name_required": "Enter your first name",
            "last_name_required": "Enter your last name",
            "invalid_email": "Enter valid email address",
            "email_not_exists": "Email address doesn't exists",
            "email_not_confirmed":"Inorder to login please confirm your email adress " + resend_link,
            "login_not_allowed": "Authentication failed",
            "incorrect_old_password": "Your old password was entered incorrecltly.",
            "unsername_numeric":"username is entirely numeric",
            'mobile_exists': 'Mobile number already exists',
            'invalid_mobile': "Invalid Mobile number"
        }
        if code in errors:
            return errors[code]
        else:
            return "Invalid data"

app_message = AppMessage()