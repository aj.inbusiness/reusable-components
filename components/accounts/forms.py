from django import forms
from django.contrib.auth import (authenticate, get_user_model, password_validation)
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm
from django.http import HttpResponse
from django.utils.translation import gettext as _
from django.core.validators import MinLengthValidator, MaxLengthValidator, RegexValidator
from accounts import validators
from .messages import app_message
from django.utils.safestring import mark_safe
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from .models import User, UserSecretToken
from django.contrib.auth.hashers import (
         check_password, is_password_usable, make_password,
     )
from django.conf import settings

class UserCreationForm(UserCreationForm):
    first_name = forms.CharField(required=False, validators=[validators.CustomizeMinLengthValidator(3), validators.CustomizeMaxLengthValidator(50), RegexValidator(r'^[a-zA-Z ]*$', app_message.errorMessages('only_letters'))], error_messages={'required':app_message.errorMessages('first_name_required')}, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'First Name'}))
    last_name = forms.CharField(required=False, validators=[validators.CustomizeMaxLengthValidator(30), RegexValidator(r'^[a-zA-Z ]*$',app_message.errorMessages('only_letters'))], widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Last Name'}))
    username = forms.CharField(validators=[MinLengthValidator(5, message=app_message.errorMessages('username_min_length')), MaxLengthValidator(50, message=app_message.errorMessages('username_max_length'))], error_messages={'unique':app_message.errorMessages('username_exists')}, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Username'}))
    email = forms.EmailField(error_messages={'unique':app_message.errorMessages('email_exists')}, widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email Address'}))
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Password'}))
    #password2 = forms.CharField(label='Confirm Password', error_messages={'password_mismatch':app_message.errorMessages('password_mismatch')}, widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Confirm Password'}))

    class Meta:
        model = User
        fields = ['email', 'username']
        #widgets = {'email': forms.EmailField(widget=forms.EmailField(attrs={'class': 'form-control'}))}
        User._meta.get_field('username').validators[0] = validators.CustomizeUncodeUsernameValidator()
        User._meta.get_field('email')._unique = True
        User._meta.get_field('first_name').validators[-1].limit_value = 255
        User._meta.get_field('mobile')._unique = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        del self.fields['password2']

    def clean_username  (self):
        username = self.cleaned_data.get('username')
        if username.isnumeric():
            raise forms.ValidationError(app_message.errorMessages('unsername_numeric'))
        return username

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        password_validation.validate_password(password1)
        return password1
    '''
    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 != password2:
            raise forms.ValidationError(app_message.errorMessages('password_mismatch'), code='password_mismatch')
        else:
            return password2
    '''

class EditUserProfileForm(UserChangeForm):

    first_name = forms.CharField(required=True, validators=[validators.CustomizeMinLengthValidator(3), validators.CustomizeMaxLengthValidator(50), RegexValidator(r'^[a-zA-Z ]*$', app_message.errorMessages('only_letters'))], error_messages={'required':app_message.errorMessages('first_name_required')}, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Name'}))
    #last_name = forms.CharField(required=False, validators=[validators.CustomizeMaxLengthValidator(30), RegexValidator(r'^[a-zA-Z ]*$',app_message.errorMessages('only_letters'))], widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Last Name'}))
    username = forms.CharField(validators=[MinLengthValidator(5, message=app_message.errorMessages('username_min_length')), MaxLengthValidator(50, message=app_message.errorMessages('username_max_length'))], error_messages={'unique':app_message.errorMessages('username_exists')}, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Username'}))
    email = forms.EmailField(error_messages={'unique':app_message.errorMessages('email_exists')}, widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email Address'}))
    #mobile = forms.CharField(validators=[MinLengthValidator(10, message=app_message.errorMessages('invalid_mobile')), MaxLengthValidator(10, message=app_message.errorMessages('invalid_mobile')), RegexValidator('^[0-9]*$',app_message.errorMessages('invalid_mobile'))], error_messages={'unique':app_message.errorMessages('mobile_exists')}, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Mobile'}))
    mobile = forms.CharField(validators=[RegexValidator('^[0-9]{10}$',app_message.errorMessages('invalid_mobile'))], error_messages={'unique':app_message.errorMessages('mobile_exists')}, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Mobile'}))    

    def __init(self, *args, **kwargs):
        super(EditUserProfileForm, self).__init__(self, *args, **kwargs)
        del self.fields['password']

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'mobile']

class ChangePasswordForm(PasswordChangeForm):

    old_password = forms.CharField(label='Old Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password1 = forms.CharField(label='New Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean_old_password(self):
        try:
            return super(ChangePasswordForm, self).clean_old_password()
        except forms.ValidationError:
            raise forms.ValidationError(app_message.errorMessages('incorrect_old_password'), code="incorrect_old_password")
    
    def clean_new_password2(self):
        try:
            return super(ChangePasswordForm, self).clean_new_password2()
        except forms.ValidationError:
            raise forms.ValidationError(app_message.errorMessages('password_mismatch'), code="password_mismatch"); 

    class Meta:
        model = User
        fields = "__all__"

class LoginForm(AuthenticationForm):

    username = forms.CharField(label="", widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username or Email address'}))
    #password = forms.CharField(label="", widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))
    remember_me = forms.BooleanField(label="Remember me", required=False, widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}))

    def confirm_login_allowed(self, user):
        remember_me = self.cleaned_data.get('remember_me')
        if not user.is_active:
            secret_token = UserSecretToken.objects.get(user=user.id)
            if secret_token.resend_count < 3:
                raise forms.ValidationError(mark_safe(app_message.errorMessages(code='email_not_confirmed', username=urlsafe_base64_encode(force_bytes(user.username)))),code='inactive')
            else:
                raise forms.ValidationError(app_message.errorMessages(code='email_not_confirmed'),code='inactive')                
        if user.is_superuser or user.is_staff:
            raise forms.ValidationError(app_message.errorMessages('login_not_allowed'), code='invalid')
        if remember_me:
            self.request.session.set_expiry(1209600) # 2 weeks 

class ChangePasswordForm(PasswordChangeForm):
    old_password = forms.CharField(label='Old Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Old Password'}))
    new_password1 = forms.CharField(label='New Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'New Password'}))
    new_password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Confirm New Password'}))
    
    def clean_new_password1(self):
        old_password = self.cleaned_data.get('old_password')
        new_password1 = self.cleaned_data.get('new_password1')

        if old_password == new_password1:
            raise forms.ValidationError("Create a new password that isn't your current password")

        if validators.NewPasswordValidator(self.user, new_password1).validate_new_password():
            raise forms.ValidationError("Create a new password you haven't used before")
        return new_password1

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    app_message.errorMessages('password_mismatch'),
                    code='password_mismatch',
                )
        password_validation.validate_password(password2, self.user)
        return password2

class ForgotPasswordForm(PasswordResetForm):
    email = forms.EmailField(label = 'Email address', error_messages = {'invalid': app_message.errorMessages('invalid_email')})

    def clean_email(self):
        email_id = self.cleaned_data['email']
        if not User.objects.filter(email__iexact=email_id, is_active=True).exists():
            raise forms.ValidationError(app_message.errorMessages('email_not_exists'))
        else:
            user = User.objects.get(email=email_id)
            if user.is_superuser or user.is_staff:
                raise forms.ValidationError(app_message.errorMessages('email_not_exists'))
        return email_id

class SetNewPasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(label="New Password")
    new_password2 = forms.CharField(label="Confirm Password", error_messages = {'password_mismatch':app_message.errorMessages('password_mismatch')})

    def clean_new_password1(self):
        new_password1 = self.cleaned_data.get('new_password1')

        if validators.NewPasswordValidator(self.user, new_password1, True).validate_new_password():
            raise forms.ValidationError("Create a new password you haven't used before")
        return new_password1

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    app_message.errorMessages('password_mismatch'),
                    code='password_mismatch',
                )
        password_validation.validate_password(password2, self.user)
        return password2

class OTPTokenForm(forms.Form):
    error_messages = {
        'only_number':'Please enter only number.',
        'len_val': 'OTP token should be 6 digit',
        'invalid_token': 'Invalid token {0} attempt left, after 3 invalid token your account will be blocked for 24 hours',
        'auth_faild':'Authentication failed, please try again',
        'error':'please try again',
        'locked': 'Your account has been locked due to the multiple invlaid attempts'
    }
    otp_token = forms.CharField(label="", validators=[MinLengthValidator(6, message=error_messages.get('len_val')), MaxLengthValidator(6, message=error_messages.get('len_val')), RegexValidator(r'^[0-9]*$', message=error_messages.get('only_number'))], widget=forms.TextInput(attrs={'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(OTPTokenForm, self).__init__(*args, **kwargs)

    def clean_otp_token(self):
        try:
            from django_otp import match_token
            from django_otp.plugins.otp_totp.models import TOTPDevice
            token = self.cleaned_data.get('otp_token')
            authenticate_user = authenticate(username=self.request.session['otp'].get('username'),password=self.request.session['otp'].get('password'))
            if authenticate_user is not None:
                device = match_token(authenticate_user, token)
                user_id = User.objects.get(username=self.request.session['otp'].get('username')).pk
                failure_count = TOTPDevice.objects.get(user_id=user_id, confirmed=True).throttling_failure_count
                max_failur_attempt = 3
                if failure_count > max_failur_attempt:
                    raise forms.ValidationError(self.error_messages.get('locked'))
                if device is None:
                    raise forms.ValidationError(self.error_messages.get('invalid_token').format(max_failur_attempt - failure_count))
            else:
                raise forms.ValidationError(self.error_messages.get('auth_faild'))
            return token
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            raise forms.ValidationError(self.error_messages.get('error'))
