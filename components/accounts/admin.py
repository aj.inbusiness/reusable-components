from django.contrib import admin
from .models import User

class AccountAdmin(admin.ModelAdmin):
	list_display = ['username','first_name', 'last_name', 'email',  'mobile', 'is_active']

admin.site.register(User, AccountAdmin)