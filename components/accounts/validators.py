from django.core.validators import MinLengthValidator, MaxLengthValidator
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.utils.translation import gettext as _
from .models import UserPasswordHistory
from django.contrib.auth.hashers import (
         check_password, is_password_usable, make_password,
     )
from .models import User


class CustomizeMinLengthValidator(MinLengthValidator):
	message = _("Enter at least %(limit_value)d characters")

class CustomizeMaxLengthValidator(MaxLengthValidator):
	message = _("Entered characters exceeds the maximum length (%(limit_value)d)")

class CustomizeUncodeUsernameValidator(UnicodeUsernameValidator):
	regex = r'^[\w._]+\Z'
	message = _("Enter a valid username. This field contain only letters, numbers, and ./_ characters")

class NewPasswordValidator:
	"""docstring for ClassName"""
	def __init__(self, user, new_password, check_with_current_password=False):
		#super(ClassName, self).__init__()
		self.user = user
		self.new_password = new_password
		self.check_with_current_password = check_with_current_password

	def validate_new_password(self):
		user = User.objects.get(id=self.user.id)
		if check_password(self.new_password, user.password):
			return True
		old_passwords = UserPasswordHistory.objects.filter(uid=self.user)
		if old_passwords is None:
			return False
		else:
			for old_password in old_passwords:
				if check_password(self.new_password, old_password.old_password):
					return True
			return False