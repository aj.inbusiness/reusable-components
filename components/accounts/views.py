from django.shortcuts import render
from django.views import View
from django.http import request, HttpResponse, HttpResponseRedirect, JsonResponse
from .forms import LoginForm, UserCreationForm, EditUserProfileForm, ChangePasswordForm, PasswordResetForm, OTPTokenForm
from django.shortcuts import render, redirect, resolve_url
from django.contrib.auth import login, authenticate, update_session_auth_hash, logout
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from .tokens import account_activation_token
from django.contrib import messages
from .models import User, UserSecretToken, UserPasswordHistory
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy
from django.contrib.auth.hashers import (check_password, is_password_usable, make_password,)
from .mails import mail
from django.conf import settings
from . import (
    is_app_installed,
    get_user_chosen_recovery_option,
    get_chosen_option_value,
    decode_chosen_option_value,
    email_option,
    mobile_option,
    get_totp_key,
    send_sms
    )
from binascii import unhexlify

class AccountAuthentication:

    session_name = 'otp'

    def get_user_by_username(self, username):
        user = User.objects.get(username=username)
        return user

class UserCreationView(View):

    def get(self, request):
        if request.user.is_anonymous:
            context = {'registration': UserCreationForm}
            return render(request, template_name='user-registration.html', context=context)
        else:
            return redirect("account:profile")

    def post(self, request):
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            username = form.cleaned_data['username']
            to_email = form.cleaned_data['email']
            if User.objects.filter(username=username).exists():
                if mail.sendEmailVerificationMail(user, request, to_email):
                    #context = {'title':'Account has been created successfully', 'message': 'Thank you. We have sent a verification mail to '+to_email+'. Please check your mail box for further details.'}
                    messages.success(request, 'Account has been created successfully. We have sent a verification mail to '+to_email+'. Please check your mail box for further details.')
                else:
                    messages.error(request, 'Problem in sending email verification.')
                    #context = {'title':'OOPS..!','message': 'Problem in sending email verification .'}
                #return render(request, template_name='message-handler.html', context=context)
                return redirect('account:message_handler')
            else:
                messages.error(request, 'Sorry something went wrong, please try again.')
                #context = {'title':'OOPS..!', 'message': 'Sorry something went wrong, please try again.'}
                #return render(request, template_name='message-handler.html', context=context)
                return redirect('account:message_handler')
        else:
            context = {'registration': form}
            return render(request, template_name="user-registration.html", context=context)

class EditUserProfileView(View):

    def get(self, request):
        editForm = EditUserProfileForm(instance=request.user)
        context = {"edit_profile_form": editForm}
        return render(request, template_name="edit-user-profile.html", context=context)

    def post(self, request):
        edit_form = EditUserProfileForm(request.POST, instance=request.user)
        if edit_form.is_valid():
            user = User.objects.get(id=self.request.user.id)
            to_email = edit_form.cleaned_data['email']
            username = edit_form.cleaned_data['username']
            if to_email != user.email:
                if mail.sendEmailVerificationMail(user, request, to_email):
                    user_profile = edit_form.save(commit=False)
                    user_profile.is_active = False
                    user_profile.save()
                    logout(request)
                    messages.success(request, 'Profile updated successfully. Since you have changed you email address, we have sent a verification mail to '+to_email+'. Please check your mail box for further details. Your account will be on in-active state, until you confirm your updated email address.')
                    return redirect('account:message_handler')
                    #context = {'title':'Your profile has been updated successfully','message': 'Since you have changed you email address, we have sent a verification mail to '+to_email+'. Please check your mail box for further details. Your account will be on in-active state, until you confirm your updated email address'}
                    #return render(request, template_name='message-handler.html', context=context)
            else:
                edit_form.save()
                if user.username != username:
                    user = User.objects.get(id=self.request.user.id)
                    mail.sendUsernameUpdatedMail(user, request, to_email)
                messages.success(request, 'Your profile has been updated successfully.')
                #return HttpResponseRedirect('registration:profile')
                return redirect('account:profile')
                #context = {'title':'Your profile has been updated successfully','message': ''}
                #return render(request, template_name='message-handler.html', context=context)
        else:
            #edit_form = UserProfileEditForm(request.POST)
            context = {"edit_profile_form": edit_form}
            return render(request, template_name="edit-user-profile.html", context=context)

def activate(request, uidb64, token, secret_token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token) and account_activation_token.check_secret_token(uid, secret_token) and user.is_active == False:
        user.is_active = True
        user.save()
        secret_token = UserSecretToken.objects.get(user_id=uid)
        secret_token.is_expired = True
        secret_token.save()
        #context = {'title':'', 'message':''}
        messages.success(request, 'Thank you, your email address has been confirmed successfully. Now you can login your account.')
        return redirect('account:message_handler')
        #return render(request, template_name='message-handler.html', context=context)
    else:
        messages.error(request,'The email confirm link was invalid, possibly because it has already been used. Please request a new email confimation.')
        return redirect('account:message_handler')
        #context = {'message': 'Invalid Request.'}
        #return render(request, template_name='message-handler.html', context=context)

class ChangePasswordPage(View):

    def get(self, request):
        change_password_form = ChangePasswordForm(request.user)
        context = {'change_password_form': change_password_form}
        return render(request, template_name='change-password.html', context=context)
    
    def post(self, request):
        change_password_form = ChangePasswordForm(request.user, request.POST)
        if change_password_form.is_valid():
            user = change_password_form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Password has been changed successfully.')
            return redirect('accounts:profile')
            #return HttpResponseRedirect(reverse('registration:profile'))
        else:
            context = {'change_password_form':change_password_form}
            return render(request, template_name="change-password.html", context=context)

class LoginView(auth_views.LoginView, AccountAuthentication):
    
    form_class = LoginForm
    template_name = 'login.html'
    extra_context = {}
    auth_level = False

    def get_form_class(self):
        if self.request.POST.get('otp_token'):
            self.form_class = OTPTokenForm
            self.auth_level = True
        else:
            self.auth_level = False
        return self.authentication_form or self.form_class

    def form_valid(self, form):
        if self.auth_level and form.cleaned_data['otp_token']:
            user = authenticate(username=self.request.session[self.session_name].get('username'),password=self.request.session[self.session_name].get('password'))
            if user is not None:
                login(self.request, user)
                del self.request.session[self.session_name]
            return HttpResponseRedirect("/")
        else:
            if is_app_installed('django_otp'):
                from django_otp import user_has_device
                user = User.objects.get(username=form.get_user())
                if user_has_device(user):
                    self.request.session[self.session_name] = {'username':form.get_user().username,'password':form.cleaned_data.get('password')}
                    self.form_class = OTPTokenForm
                    return render(self.request, self.template_name, self.get_context_data(first_load=True, username=form.get_user().username))
                else:
                    login(self.request, form.get_user())
                    return HttpResponseRedirect("/")
            else:
                login(self.request, form.get_user())
                return HttpResponseRedirect("/")
    
    def get_success_url(self):
        return reverse_lazy("account:profile")
    success_url = reverse_lazy("account:profile")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['first_load'] = kwargs.pop('first_load', None)
        if self.session_name in self.request.session:
            username = self.request.session[self.session_name].get('username')
            user = self.get_user_by_username(username)
            chosen_option = get_user_chosen_recovery_option(user)
            chosen_option_value = get_chosen_option_value(user, chosen_option)
            if chosen_option == 'email':
                chosen_option_text = "email address"
            elif chosen_option == 'mobile':
                chosen_option_text = "mobile number"
            context['chosen_option_text'] = chosen_option_text 
            context['chosen_option_value'] = decode_chosen_option_value(chosen_option_value)
            
        return context


class PasswordChangeView(auth_views.PasswordChangeView):
    success_url = reverse_lazy('account:change-password-done')

    def form_valid(self, form):
        save_user_old_password(self.request.user, old_password=form.cleaned_data.get('old_password'))
        mail.sendPasswordChangedConfirmationMail(self.request.user, self.request, self.request.user.email)
        return super().form_valid(form)

class PasswordResetView(auth_views.PasswordResetView):
    success_url = reverse_lazy("account:password_reset_done")
    
    def get(self, request, *args, **kwargs):
        """Handle GET requests: instantiate a blank version of the form."""
        if request.user.is_authenticated:
            return redirect("account:profile")
        return self.render_to_response(self.get_context_data())

class SetNewPasswordView(auth_views.PasswordResetConfirmView):
    success_url = reverse_lazy("account:password_reset_complete")
    post_reset_login = False

    def form_valid(self, form):
        mail.sendPasswordChangedConfirmationMail(self.user, self.request, self.user.email)
        return super().form_valid(form)

class PasswordResetCompleteView(auth_views.PasswordResetCompleteView):
    pass

def resend_email_verification_link(request, username):
    username = force_text(urlsafe_base64_decode(username))
    if User.objects.filter(username=username, is_active=False).exists():
        user = User.objects.get(username=username)
        token_count = UserSecretToken.objects.get(user=user.id)
        if token_count.resend_count < 3:        
            to_email = user.email
            if mail.sendEmailVerificationMail(user, request, to_email):
                messages.success(request, 'The email confirmation link was sent to '+to_email+'. Please check your mail box for further details. Your account will be on in-active state, until you confirm your email address.')
            else:
                messages.error(request,'The email confirm link was not sent invalid, please try again later.')
        else:
            messages.error(request,'Invalid request, please try again later.')    
    else:
        messages.error(request,'Invalid request, please try again later.')
    return redirect('account:message_handler')

def save_user_old_password(user, old_password):
    old_password = UserPasswordHistory(uid=user, old_password=make_password(old_password))
    old_password.save()
    return True

class LostTwoFactorAuthDevice(View, AccountAuthentication):
    
    def get(self, request):
        pass

    def post(self, request):
        username = request.session[self.session_name].get('username')
        
        user = self.get_user_by_username(username)
        chosen_option = get_user_chosen_recovery_option(user)
        from django_otp.oath import totp
        token = None
        key = get_totp_key(user)
        response = {}
        if key is not None:
            token = totp(unhexlify(key.encode()))
            message = str(token) + " is you verification code."
            chosen_option_value = get_chosen_option_value(user, chosen_option)
            if chosen_option == email_option:
                mail.send_totp_mail(token, chosen_option_value)
                response['status'] = True
                response['to'] = chosen_option_value
            elif chosen_option == mobile_option:
                if send_sms(chosen_option_value, message):
                    response['status'] = True
                    response['to'] = chosen_option_value
                else:
                    response['status'] = False
        else:
            response['error'] = True
        return JsonResponse(response)