from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.urls import reverse
from .views import UserCreationView, activate, EditUserProfileView, ChangePasswordPage, PasswordChangeView, PasswordResetView, SetNewPasswordView, LoginView, resend_email_verification_link, PasswordResetCompleteView
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from .forms import ForgotPasswordForm, SetNewPasswordForm, ChangePasswordForm, LoginForm
from django.views.generic.base import TemplateView

app_name= 'account'

urlpatterns = [
    path('registration/', UserCreationView.as_view(), name='sign-up'),
    path('confirm-email/<uidb64>/<token>/<secret_token>', activate, name='activate'),
    path('login/', LoginView.as_view(redirect_authenticated_user=True,), name="sign-in"),
    path('logout/', auth_views.LogoutView.as_view(next_page='account:sign-in'), name="logout"),
    path('profile/edit', login_required(EditUserProfileView.as_view()), name="profile"),
	path('password/change', login_required(PasswordChangeView.as_view(template_name="change-password.html", form_class=ChangePasswordForm), login_url='account:sign-in'), name='change-password'),
	path('password/change/done/', login_required(auth_views.PasswordChangeDoneView.as_view(template_name="change-password-done.html")), name='change-password-done'),
    path('password/reset/', PasswordResetView.as_view(template_name='password-reset.html', form_class=ForgotPasswordForm, email_template_name='password-reset-email.html'), name="forgot-password"),
    path('password/reset/email/done',auth_views.PasswordResetDoneView.as_view(template_name="password-reset-email-done-message.html"), name="password_reset_done"),
    path('password/reset/confirm/<uidb64>/<token>',SetNewPasswordView.as_view(template_name="set-new-password.html", form_class=SetNewPasswordForm), name='password_reset_confirm'),
    path('password/reset/complete/', PasswordResetCompleteView.as_view(template_name="password-reset-complete.html"), name="password_reset_complete"),
    path('request/status', TemplateView.as_view(template_name="message-handler.html"), name="message_handler"),
    path('resend/confirm-email/<username>', resend_email_verification_link , name='resend_confirm_email'),
]

try:
    from .views import LostTwoFactorAuthDevice
    urlpatterns.append(path('lost-device/', LostTwoFactorAuthDevice.as_view(), name="lost_device"))
except:
    pass