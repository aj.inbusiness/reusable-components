from django.conf import settings
import urllib.request
import urllib.parse
import json

email_option = "email"
mobile_option = "mobile"

def is_app_installed(app_name):

    if app_name in settings.INSTALLED_APPS:
        return True
    else:
        return False

def get_user_chosen_recovery_option(user):
    try:
        from two_factor_authentication.models import ChosenRecoveryMethod
        chosen_option = ChosenRecoveryMethod.objects.get(user_id=user.id).chosen_option
        return chosen_option
    except:
        return None

def get_chosen_option_value(user, chosen_option):
        option_value = chosen_option == email_option and user.email or (hasattr(user, mobile_option) and user.mobile or None)
        return option_value

def decode_chosen_option_value(option_value):
    option_len = len(option_value)
    decode_string = "*"
    if option_value.isnumeric():
        show = 2
        option_value = (decode_string * (option_len-show)) + option_value[option_len-show:option_len]
    else:
        position = option_value.find('@')
        till = position - 2
        option_value = option_value[0] + (decode_string * (int(till))) + option_value[till+1:position] + option_value[position:option_len]
    return option_value

def get_totp_key(user):
    try:
        from django_otp.plugins.otp_totp.models import TOTPDevice
        key = TOTPDevice.objects.get(user_id=user.id).key
        return key
    except:
        return None


def send_sms(number, message):
    apikey = 'EC28M2FgjL8-v45PpkRDTOu9xxAjdoKRtesSphgFEV'
    sender = 'SARNGI'
    data = urllib.parse.urlencode({'apikey': apikey, 'numbers': number, 'sender':sender, 'message': message })
    request = urllib.request.Request("https://api.textlocal.in/send/?")
    response = urllib.request.urlopen(request, data.encode('utf-8'))
    sms = response.read()
    json_response = json.loads(sms.decode('utf-8'))
    if json_response['status'] == "success":
        return True
    else:
        return False