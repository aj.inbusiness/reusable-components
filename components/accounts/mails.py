
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from .tokens import account_activation_token
from django.core.mail import EmailMessage, send_mail
from django.utils.html import strip_tags

class Mail:

	def __init__(self, *args, **kwargs):
		pass

	def sendEmailVerificationMail(self, user, request, to_email):

	    current_site = get_current_site(request)
	    mail_subject = 'Confirm Your Email Address'
	    message = render_to_string('mail/account-activation-email.html', {
	        'user': user,
	        'domain': current_site.domain,
	        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
	        'token': account_activation_token.make_token(user),
	        'secret_token': account_activation_token.make_secret_token(user),
	        'protocol': request.is_secure() and 'https' or 'http',
	    })
	    email = EmailMessage(mail_subject, message, to=[to_email])
	    email.content_subtype = "html"
	    if email.send():
	        return True
	    else:
	        return False

	def sendPasswordChangedConfirmationMail(self, user, request, to_email):
		current_site = get_current_site(request)
		mail_subject = "Your account password has been changed"	
		html_message = render_to_string('mail/password-changed-cofirmation-email.html',{
			"user": user,
			"message_content": "",
			"domain":current_site.domain,
			"protocol": request.is_secure() and 'https' or 'http',
		})

		text_message = strip_tags(html_message)
		send_mail(mail_subject, text_message, '', [to_email], fail_silently=False, html_message=html_message)

	def sendUsernameUpdatedMail(self, user, request, to_email):
		current_site = get_current_site(request)
		mail_subject = "Your account username has been changed"	
		html_message = render_to_string('mail/username-changed-cofirmation-email.html',{
			"user": user,
			"message_content": "",
			"domain":current_site.domain,
			"protocol": request.is_secure() and 'https' or 'http',
		})

		text_message = strip_tags(html_message)
		send_mail(mail_subject, text_message, '', [to_email], fail_silently=False, html_message=html_message)

	def send_totp_mail(self, totp, to_email):
		mail_subject = "Verification Code for sign-in"	
		html_message = render_to_string('mail/totp-email.html',{
			"totp": totp,
		})
		text_message = strip_tags(html_message)
		send_mail(mail_subject, text_message, '', [to_email], fail_silently=False, html_message=html_message)

mail = Mail()