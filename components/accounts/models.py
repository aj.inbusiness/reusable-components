from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
	mobile = models.CharField(max_length=12, null=True)

class UserSecretToken(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    secret_token = models.CharField(max_length=255,null=True)
    is_expired = models.BooleanField(default=False)
    resend_count = models.IntegerField(default=0)
    created_on = models.DateTimeField(auto_now_add=True, null=True)

class UserPasswordHistory(models.Model):
	uid = models.ForeignKey(User, on_delete=models.CASCADE)
	old_password = models.TextField()
	changed_on = models.DateTimeField(auto_now_add=True, null=True)
