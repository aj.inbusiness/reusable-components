$(function() {
	var form_id = "try_another_way_form";
	var button_text = $('#'+form_id).find('button').text()
	var loader = '<span class="spinner-grow spinner-grow-sm text-primary" role="status" aria-hidden="true"></span> Loading...'

    $(document).on('click', 'a.lost-device', function(event) {
    	/* event.preventDefault()
    	$("#authentication-form, .chosen_two_factor_recovery_option").toggle()
    	$('.alert').remove();
    	var chosen_option = $(button).attr('data-chosen-option');
    	$(button).text(button_text)
        */
    });
    $('#tryAnotherModalCenter').on('shown.bs.modal', function() {
        $('.alert').remove();
    })
    $(document).on('submit', '#'+form_id, function(e) {
            e.preventDefault();
        	console.log("test")
            $('.alert').remove()
            var form = $(this).get(0)
            var formData = new FormData(form)
        	var end_point = $(this).attr('action')
            console.log(form)
            var html = '<div class="alert"><a class="close" href="#" data-dismiss="alert">×</a>'
            var class_name = ""
            $.ajax({
                type:'POST',
                url:end_point,
                data:formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend:function() {
                    $(form).find('button').prop('disabled', true);
		            $(form).find('button').html(loader)
                },
                success:function(response) {
                	console.log(response)
                    if (response.status == true) {
                    	html += '<span class="text-green">Verifcation code has been sent to - <strong>'+response.to+'</strong>.</span>'
                    	class_name = "alert-success"
                        $('.ajax-success-reponse').append(html).find('.alert').addClass(class_name)
                        $("#tryAnotherModalCenter").modal("hide");
                    }
                    else {
                    	class_name = "alert-danger"
                    	html += '<span class="">Technical issue, please try again.</span>'
                    }
                    html += '</div>'
                },
                error : function(xhr,errmsg,err) {
                    html += '<span class="">Technical issue, please try again.</span>'
                	html += '</div>'
                    class_name = "alert-danger"
            	},
            	complete:function() {
                    $(form).find('button').prop('disabled', false);
                    $(form).find('button').text(button_text)
            	    $('.ajax-error-reponse').append(html).find('.alert').addClass(class_name)
                }
            });
        });
});