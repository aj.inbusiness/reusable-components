$(document).ready(function () {
  $(window).on("load", function () {
    $(".section-loader").show();
    $(".section-loader").fadeOut("2000");
  });
  /*
    | ==========================
    | NAV FIXED ON SCROLL
    | ==========================
    */
  $(window).on("scroll", function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
      $(".nav-scroll").addClass("nav-strict");
    } else {
      $(".nav-scroll").removeClass("nav-strict");
    }
  });
  $("#ap-header").onePageNav({
    currentClass: "active",
    changeHash: false,
    scrollSpeed: 750,
    scrollThreshold: 0.5,
  });
  $(document).on("scroll", onScroll);
});
function onScroll(event) {
  var scrollPos = $(window).scrollTop()+50;
  $(".navbar-nav li").each(function () {
    var currLink = $(this);
    var refElement = $(currLink.find("a").attr("href"));
    if (
      refElement.position().top <= scrollPos &&
      refElement.position().top + refElement.height() > scrollPos
    ) {
      $(".navbar-nav li").removeClass("active");
      currLink.addClass("active");
    } else {
      currLink.removeClass("active");
    }
  });
}
// Smooth Scroll
$(function () {
  $('a[href]:not([href="\\#"])').click(function () {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop: target.offset().top,
          },
          600
        );
        return false;
      }
    }
  });
});
var trigger = $(".navbar-toggler"),
  overlay = $(".overlay"),
  navc = $(".navbar-collapse"),
  active = false;
navli = $(".navbar-nav li");

$(".navbar-toggler, .navbar-nav li a, .overlay").on("click", function () {
  $(".navbar-toggler").toggleClass("active");
  $(this).parent("li").toggleClass("active").siblings().removeClass("active");
});
var wow = new WOW({
    mobile: false  // trigger animations on mobile devices (default is true)
});
wow.init();