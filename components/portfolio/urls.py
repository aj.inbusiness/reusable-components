from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView

app_name= 'portfolio'

urlpatterns = [
    path('ajith', TemplateView.as_view(template_name="index.html"), name='portfolio_index'),
]