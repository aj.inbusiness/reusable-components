(function($) {
    'use strict';
    $(document).ready(function() {
    	$("#reply_btn").on("click", function(){
    		$(".reply_area").toggle()
    		if ($(".reply_area").is(":hidden")) {
    			$(this).text("Reply")
    		}
    		else {
    			$(this).text("Cancel")
    		}
    	})

    });
})(django.jQuery);