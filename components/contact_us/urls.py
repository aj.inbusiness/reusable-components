from django.urls import path
from .views import ContactUsView
from django.contrib import admin

app_name = "contact_us"

urlpatterns = [
				path('contact-us/',ContactUsView.as_view(), name="contact_us")
]
