# Generated by Django 3.0.2 on 2020-04-15 22:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact_us', '0007_auto_20200415_1931'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactus',
            name='admin_reply_message',
            field=models.TextField(null=True, verbose_name='Admin Reply'),
        ),
    ]
