from django.contrib import admin
from .models import ContactUs
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags


class ContactUsAdmin(admin.ModelAdmin):
	list_display = ['name', 'email', 'mobile_num', 'created_on', 'is_replied']
	list_filter = ['is_replied']
	search_fields = ['name','email', 'mobile_num', 'subject', 'message']
	readonly_fields = ['name', 'email', 'mobile_num', 'subject', 'message', 'is_replied']
	
	def get_readonly_fields(self, request, obj=None):
		if obj.is_replied:
			return self.readonly_fields + ['admin_reply_message']
		return self.readonly_fields

	fields = ['name','email', 'mobile_num', 'subject', 'message','is_replied','admin_reply_message']

	change_form_template = 'admin/contact_us/change_form.html'

	#fieldsets = (('Advanced options', {'classes': ('collapse',),'fields': ('mobile_num', 'email'),}),)

	def has_add_permission(self, request):
		return False

	def has_save_permission(self, request, obj=None):
		return False

	def has_delete_permission(self, request, obj=None):
		if not request.user.is_superuser:
			return False
		return True
	def has_change_permission(self, request, obj=None):
		return True

	def changelist_view(self, request, extra_context=None):
		extra_context = extra_context or {}
		extra_context['title']= "Enquiries"
		return super(ContactUsAdmin, self).changelist_view(request, extra_context=extra_context)

	def changeform_view(self, request, object_id, form_url='', extra_context=None):
		extra_context = extra_context or {}
		if ContactUs.objects.filter(id=object_id, is_replied=True).exists():	
			extra_context['show_save'] = False
		else:
			extra_context['show_save'] = True
		extra_context['show_save_and_continue'] = False
		extra_context['show_save_and_add_another'] = False
		user = ContactUs.objects.get(id=object_id)
		extra_context['title'] = "Reply to " +user.name+  "'s Query"
		return super(ContactUsAdmin, self).changeform_view(request, object_id, extra_context=extra_context)
	
	def save_model(self, request, obj, form, change):
		obj.is_replied = True
		user_mail_subject = "Clarification for your Query"	
		html_message = render_to_string('contactus-reply-to-user-mail.html',{'name': obj.name, 'message_content': form.cleaned_data.get('admin_reply_message')})
		text_message = strip_tags(html_message)
		send_mail(user_mail_subject, text_message, 'Dont Reply <noreplym2softtech@gmail.com>', [obj.email], fail_silently=False, html_message=html_message)
		super().save_model(request, obj, form, change)

admin.site.register(ContactUs, ContactUsAdmin)

