from django.db import models

class ContactUs(models.Model):
	name = models.CharField('Name', max_length=100, null=True)
	email = models.EmailField('Email', max_length=100, null=True)
	mobile_num = models.CharField('Mobile', max_length=10, null=True)
	subject = models.CharField('Subject', max_length=100, null=True)
	message = models.TextField('Message', max_length=1500, null=True)
	admin_reply_message = models.TextField('Reply Message', null=True)
	created_on = models.DateTimeField('created On', auto_now_add=True, null=True)
	is_replied = models.BooleanField('Is Replied', default=0)

	def __str__(self):
		return self.name