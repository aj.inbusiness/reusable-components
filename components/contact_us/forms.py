from django import forms
from accounts import validators
from django.core.validators import MinLengthValidator, MaxLengthValidator, RegexValidator

class ContactUsForm(forms.Form):
	name = forms.CharField(label='', validators=[MinLengthValidator(5, message="Name should be grater than %(limit_value)d letters"), MaxLengthValidator(40, message="Name should lesser than %(limit_value)d letters"), RegexValidator(r'^((?:[A-Za-z ]{2,})+)(.*)$', message="Invalid name")], widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}))
	email = forms.EmailField(label='', widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'example@domain.com'}))
	mobile_num = forms.CharField(label='', validators=[RegexValidator(r'^[0-9]{10}$', message="Invalid mobile")], widget=forms.NumberInput(attrs={'class':'form-control', 'placeholder':'9999999999'}))
	subject = forms.CharField(label='', validators=[MinLengthValidator(10, message="Subject should be grater than %(limit_value)d letters"), MaxLengthValidator(200, message="Subject should be lesser than %(limit_value)d letters"), RegexValidator(r'^(?!-)(?!.*--)(?!.*  -)(?!.*  )[A-Za-z0-9- ]+(?<!-)*$', message="Invalid subject")], widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Subject'}))
	message_content = forms.CharField(label='', min_length=10, max_length=1500, validators=[RegexValidator(r'^(?!-)(?!.*--)(?!.*    -)(?!.*  )[A-Za-z0-9- _]+(?<!-)*$', message="Invalid message")], widget=forms.Textarea(attrs={'class': 'form-control message', 'id': 'message', 'placeholder': 'Write your mesage here...'}))


