from django.shortcuts import render
from django.views import View
from .forms import ContactUsForm
from .models import ContactUs
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse
from django.core.mail import send_mail, mail_admins
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.contrib.sites.shortcuts import get_current_site

class ContactUsView(View):

	def get(self, request):
		if request.user.is_authenticated:
			initial = {'name': request.user.first_name, 'email':request.user.email}
		else:
			initial = {}
		form = ContactUsForm(initial=initial)
		context = {'form': form}
		return render(request, template_name = "contact-us.html", context={'form': form})

	def post(self, request):
		form = ContactUsForm(request.POST)
		
		if form.is_valid():
			name = form.cleaned_data.get('name')
			to_mail = form.cleaned_data.get('email')
			mobile_num = form.cleaned_data.get('mobile_num')
			subject = form.cleaned_data.get('subject')
			message_content = form.cleaned_data.get('message_content')
			contact_us = ContactUs(name=name, email=to_mail, mobile_num=mobile_num, subject=subject, message=message_content)
			contact_us.save()
			user_mail_subject = "Thanks for reching us."
			
			html_message = render_to_string('contactus-user-mail.html',{'name': name, 'message_content': 'Thank you, We have recevied your mail and we will get back to you soon.'})
			text_message = strip_tags(html_message)
			send_mail(user_mail_subject, text_message, 'Dont Reply <noreplym2softtech@gmail.com>', [to_mail], fail_silently=False, html_message=html_message)
			domain = request.build_absolute_uri('/admin/contact_us/contactus/'+str(contact_us.id)+'/change/')
			admin_html_message = render_to_string('contactus-admin-mail.html', {'domain':domain, 'id':contact_us.id, 'name':name,'message_content':'There is a new enquiry request from user.'})
			admin_text_message = strip_tags(admin_html_message)
			admin_mail_subject = "New enquiry request"
			mail_admins(admin_mail_subject, admin_text_message, fail_silently=False, html_message=admin_html_message)
			messages.success(request, "Thank you " +name+ " we will get back you soon.")
			return HttpResponseRedirect(reverse("contact_us:contact_us"))
		else:
			return render(request, template_name="contact-us.html", context={'form': form})
