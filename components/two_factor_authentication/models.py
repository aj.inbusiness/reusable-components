from django.db import models
from accounts.models import User

class ChosenRecoveryMethod(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	chosen_option = models.CharField(max_length=10)