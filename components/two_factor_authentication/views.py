from django.shortcuts import render, redirect, reverse
from django.views import View 
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django_otp.plugins.otp_totp.models import TOTPDevice, default_key
from .forms import RegisterTwoFactorAuthenticatorForm, TwoFactorAuthenticatorRecoveryMethodForm
from django.contrib import messages
from django_otp import user_has_device
from .mails import mail
from .models import ChosenRecoveryMethod
from django.views.generic.edit import FormView
from . import email_option_name, mobile_option_name

class TwoFactor:

	route = "/"

	def check_two_factor(self, user, confirmed=True):
		device_id = self.get_device_id(user)
		if TOTPDevice.objects.filter(user_id=user.id, pk=device_id, confirmed=confirmed).exists():
			return True
		else:
			return False

	def add_device(self, user):
		device = TOTPDevice.objects.create(name=user.username, confirmed=False, key=default_key(), step=30, t0=0, digits=6, tolerance=1, drift=0, last_t=0, user_id=user.id, throttling_failure_count=0)
		device.save()
		return device.id

	def get_device_id(self, user):
		device = TOTPDevice.objects.get(user_id=user.id)
		return device.id

	def register_two_factor(self, user):

		device_id = self.get_device_id(user)
		device = TOTPDevice.objects.get(pk=device_id, user_id=user.id)
		device.confirmed = True
		device.save()
		return True

	def save_chosen_recovery_method(self, user, chosen_option):
		recovery_option = ChosenRecoveryMethod.objects.create(user=user, chosen_option=chosen_option)
		recovery_option.save();
		return True
	
	def update_chosen_recovery_method(self, user, chosen_option):
		recovery_option = ChosenRecoveryMethod.objects.get(user=user.id)
		recovery_option.chosen_option = chosen_option	
		recovery_option.save();
		return True

class TwoFactorAuthentication(View, TwoFactor):
	template_name = "account.html"

	def get(self, request):
		
		if not request.user.is_authenticated:
			return redirect("/")
		
		context = {}
		context['enabled'] = False
		
		if user_has_device(request.user):
			context['enabled'] = True
		return render(request, template_name=self.template_name, context=context)

class RegisterTwoFactorAuthentication(View, TwoFactor):
	template_name = "two-factor-authentication.html"
	
	def get(self, request):
		
		if not request.user.is_authenticated:
			return redirect("/")

		context = {}
		if user_has_device(request.user):
			context['enabled'] = True

		elif user_has_device(request.user, False):
			context['device_id'] = self.get_device_id(request.user)
			context['rc_form'] = TwoFactorAuthenticatorRecoveryMethodForm(request.user)
			context['form'] = RegisterTwoFactorAuthenticatorForm(request.user)
		
		else:
			context['device_id'] = self.add_device(request.user)
			context['rc_form'] = TwoFactorAuthenticatorRecoveryMethodForm(request.user)
			context['form'] = RegisterTwoFactorAuthenticatorForm(request.user)
		
		return render(request, template_name=self.template_name, context=context)

	def post(self, request):
		
		form = RegisterTwoFactorAuthenticatorForm(request.user, request.POST)
		rc_form = TwoFactorAuthenticatorRecoveryMethodForm(request.user, request.POST)
		context = {}
		if form.is_valid() and rc_form.is_valid():
			token = form.cleaned_data['pin_code']
			chosen_option = rc_form.cleaned_data['recovery_method']
			if self.register_two_factor(request.user):
				messages.success(request, "Congratulations! You have enabled Two-factor Authentication!")
				self.save_chosen_recovery_method(request.user, chosen_option)
				mail.send_enabled_2fa_confirmation_mail(request)
				return redirect(reverse('two_factor_authentication:profile_account'))
			else:
				messages.error(request, "Failed! You not have enabled Two-factor Authentication!")
				return redirect(reverse('two_factor_authentication:profile_account'))
		else:
			context['form'] =  form
			context['device_id'] = self.get_device_id(request.user)
			return render(request, template_name=self.template_name, context=context)

class DisableTwoFactorAuthentication(View, TwoFactor):

	def get(self, request):
		
		if not request.user.is_authenticated:
			return redirect("/")

		redirect_route = "two_factor_authentication:profile_account"

		if user_has_device(request.user):
			device_id = self.get_device_id(request.user)
			device = TOTPDevice.objects.get(pk=device_id, user_id=request.user.id)			
			device.delete()
			chosen_option = ChosenRecoveryMethod.objects.get(user=request.user.id)
			chosen_option.delete()
			#messages.success(request, "Thank you! You have disabled Two-factor Authentication!")
			return redirect(reverse(redirect_route))
		else:
			return redirect(reverse(redirect_route))

def qrcode_view(request, deviceid):

	if not request.user.is_authenticated:
		return redirect("/")

	device = TOTPDevice.objects.get(pk=deviceid, user_id=request.user.id, confirmed=False)
	if device is not None:
		try:
			import qrcode
			import qrcode.image.svg
			img = qrcode.make(device.config_url, image_factory=qrcode.image.svg.SvgImage)
			response = HttpResponse(content_type='image/svg+xml')
			img.save(response)
		except ImportError:
			response = HttpResponse('', status=503)
	else:
		response = HttpResponse('', status=503)
	return response

class RecoverMethodView(FormView, TwoFactor):
	form_class = TwoFactorAuthenticatorRecoveryMethodForm
	template_name = "recovery_options.html"
	success_url = "/profile/account/two-factor-authentication"

	def form_valid(self, form):
		chosen_option = form.cleaned_data['recovery_method']
		if chosen_option == email_option_name or chosen_option == mobile_option_name:
			messages.success(self.request, 'Changes has been updated.')
			self.update_chosen_recovery_method(self.request.user, chosen_option)
			#mail.send_enabled_2fa_confirmation_mail(request)
		return HttpResponseRedirect(self.success_url)

	def get_form_kwargs(self):
		user = self.request.user
		form_kwargs = super(RecoverMethodView, self).get_form_kwargs()
		form_kwargs.update({
            'initial': {
                'option': ChosenRecoveryMethod.objects.get(user_id=user.id),
                'email_option_name':email_option_name,
                'mobile_option_name':mobile_option_name,
            }
        })
		return form_kwargs