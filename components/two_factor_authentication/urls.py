from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.views import LoginView
from django_otp.forms import OTPAuthenticationForm
from .views import TwoFactorAuthentication, RegisterTwoFactorAuthentication, DisableTwoFactorAuthentication, qrcode_view, RecoverMethodView
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required

app_name= 'two_factor_authentication'

urlpatterns = [
    path('otp/login', LoginView.as_view(redirect_authenticated_user=True, authentication_form=OTPAuthenticationForm, template_name="otp_login.html")),
    path('profile/account', login_required(TwoFactorAuthentication.as_view()), name="profile_account"),
    path('profile/account/two-factor-authentication', login_required(RegisterTwoFactorAuthentication.as_view()), name="register_two_factor"),
    path('profile/<deviceid>/qrcode', login_required(qrcode_view), name="user_qrcode"),
   	path('profile/account/disable-two-factor-authentication', login_required(DisableTwoFactorAuthentication.as_view()), name="disable_two_factor"),
   	path('profile/account/two-factor-authentication/method', login_required(RecoverMethodView.as_view()), name="recovery_two_factor"),
]