
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.core.mail import EmailMessage, send_mail
from django.utils.html import strip_tags

class Mail:

	def __init__(self, *args, **kwargs):
		pass

	def send_enabled_2fa_confirmation_mail(self, request):
		current_site = get_current_site(request)
		mail_subject = "Two-Factor Authentication enabled successfully"	
		html_message = render_to_string('mail/enabled-two-factor-authentication.html',{
			"user": request.user,
			"message_content": "",
			"domain":current_site.domain,
			"protocol": request.is_secure() and 'https' or 'http',
		})
		to_email = request.user.email
		text_message = strip_tags(html_message)
		send_mail(mail_subject, text_message, '', [to_email], fail_silently=False, html_message=html_message)


mail = Mail()