from django import forms
from django.core.validators import RegexValidator
from django_otp.forms import OTPAuthenticationForm, OTPTokenForm
from django_otp import devices_for_user
from accounts.models import User
from .models import ChosenRecoveryMethod

class RegisterTwoFactorAuthenticatorForm(forms.Form):
	error_message = {
			'invalid_token': 'Invalid token, please try again',
			'only_numbers': 'Only numbers are allowed',
	}

	pin_code = forms.CharField(validators=[RegexValidator('^[0-9]{6}$', message=error_message.get('only_numbers'))], widget=forms.TextInput(attrs={'class':'form-control', 'autocomplete':'off'}))
	
	def __init__(self, user=None, *args, **kwargs):
		self.user = user
		super(RegisterTwoFactorAuthenticatorForm, self).__init__(*args, **kwargs)
		
	def clean_pin_code(self):
		token = self.cleaned_data.get('pin_code')
		device = self.validate_token(self.user, token)
		if device is None:
			raise forms.ValidationError(self.error_message.get('invalid_token'))
		return token

	def validate_token(self, user, token):
		matches = (device for device in devices_for_user(user, False) if device.verify_token(token))
		return next(matches, None)

class TwoFactorAuthenticatorRecoveryMethodForm(forms.Form):
	error_message = {
			'only_numbers': 'Only numbers are allowed',
			'rec_required': 'Please select recovery method',
			'error': 'something went wronge'
	}
	
	def __init__(self, user=None, *args, **kwargs):
		self.user = user
		super(TwoFactorAuthenticatorRecoveryMethodForm, self).__init__(*args, **kwargs)
		user_email = hasattr(self.user, 'email') and self.user.email or None
		user_mobile = hasattr(self.user, 'mobile') and self.user.mobile or None
		self.choices_1 = 'mobile'
		self.choices_2 = 'email' 
		CHOICES = [
			(self.choices_1, user_mobile), (self.choices_2, user_email)
		]
		self.fields['recovery_method'] = forms.ChoiceField(choices=CHOICES, initial='', widget=forms.RadioSelect(attrs={'class':'form-check-input'}))

	def clean_recovery_method(self):
		recovery_method = self.cleaned_data.get('recovery_method')
		if recovery_method is None:
			raise forms.ValidationError(self.error_message.get('rec_required'))
		return recovery_method

